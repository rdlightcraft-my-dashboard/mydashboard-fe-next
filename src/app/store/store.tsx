'use client';

import { configureStore } from '@reduxjs/toolkit';
import billsReducer from '@/features/bills/billsSlice';
import userSlice from '@/features/users/userSlice';

export const store = configureStore({
    reducer: {
        bills: billsReducer,
        users: userSlice,
    },
});
