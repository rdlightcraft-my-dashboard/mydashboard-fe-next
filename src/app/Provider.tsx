'use client';

import {Provider} from 'react-redux';
import {store} from './store/store';
import { Amplify } from 'aws-amplify';
import config from '../../amplifyconfiguration.json';
Amplify.configure(config);

function Providers({children}: { children: React.ReactNode }) {
    return <Provider store={store}>{children}</Provider>

}

export default Providers;