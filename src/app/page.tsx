import React from 'react';
import Header from "@/app/components/layout/Header";
import Footer from "@/app/components/layout/Footer";
import AuthenticateUser from "@/app/context/AuthenticateUser";
import LoginModal from "@/features/users/LoginModal";


const Home = () => {
    return (
        <AuthenticateUser>
            <Header/>
            <LoginModal/>
            <Footer/>
        </AuthenticateUser>
    );
};

export default Home;