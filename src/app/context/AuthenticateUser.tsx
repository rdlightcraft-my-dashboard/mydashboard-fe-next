'use client'
// Import React and React types
import React, { ReactNode, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { setUserRole, setUsername } from '@/features/users/userSlice';
import { fetchAuthSession, getCurrentUser } from '@aws-amplify/auth';
import { Hub } from 'aws-amplify/utils';
import { toast } from 'sonner';

// Define the type for your component's props
interface AuthenticateUserProps {
    children: ReactNode;
}

const AuthenticateUser: React.FC<AuthenticateUserProps> = ({ children }) => {
    const dispatch = useDispatch();

    Hub.listen('auth', (data) => {
        switch (data.payload.event) {
            case 'signedIn':
                dispatch(setUsername(data.payload.data.username));
                break;
            case 'signedOut':
                dispatch(setUsername(null));
                break;
            default:
                break;
        }
    });

    useEffect(() => {
        async function currentSession() {
            try {
                const { username } = await getCurrentUser();
                dispatch(setUsername(username));

                const { idToken } = (await fetchAuthSession()).tokens ?? {};
                const userRoles = idToken?.payload['cognito:groups'];

                dispatch(setUserRole(userRoles ?? ['user']));
            } catch (err) {
                toast.error('Unauthorized!');
                console.error(err);
            }
        }

        currentSession();
    }, [dispatch]);

    return <>{children}</>;
};

export default AuthenticateUser;