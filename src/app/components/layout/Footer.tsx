import React from 'react';

const Footer = () => {
    return (
        <footer className="relative md:fixed z-[100] inset-x-0 bottom-0 bg-white border-t text-center py-4">
            <p className="text-sm">© 2023 My Dashboard. All rights reserved.</p>
        </footer>
    );
};

export default Footer;