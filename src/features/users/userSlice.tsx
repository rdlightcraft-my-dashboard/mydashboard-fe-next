'use client'
import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    username: null,
    role: ['user'],
};

const userSlice = createSlice({
    name: 'users',
    initialState,
    reducers: {
        setUsername: (state, action) => {
            state.username = action.payload;
        },
        setUserRole: (state, action) => {
            state.role = action.payload;
        },
    },
});

export const { setUsername, setUserRole } = userSlice.actions;

export default userSlice.reducer;
