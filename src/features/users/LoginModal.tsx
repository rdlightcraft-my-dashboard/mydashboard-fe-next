'use client'
import { Button } from '@/components/ui/button';
import {
    Dialog,
    DialogContent,
    DialogHeader,
    DialogTrigger,
} from '@/components/ui/dialog';
import { Authenticator} from '@aws-amplify/ui-react';

const LoginModal = () => {

    return (
        <Dialog>
            <DialogTrigger asChild>
                <Button size="sm">Login</Button>
            </DialogTrigger>
            <DialogContent className="max-w-[330px] md:max-w-fit">
                <DialogHeader></DialogHeader>
                <Authenticator>
                </Authenticator>
            </DialogContent>
        </Dialog>
    );
};

export default LoginModal;