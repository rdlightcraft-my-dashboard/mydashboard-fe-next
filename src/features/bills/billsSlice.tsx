import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    bills: [],
    loading: false,
    error: null,
    billTableLastUpdate: '',
};

const billsSlice = createSlice({
    name: 'bills',
    initialState,
    reducers: {
        setBills: (state, action) => {
            state.bills = action.payload;
            state.loading = false; // Optionally reset loading state here
            state.error = null; // Optionally reset error state here
        },
        setLoading: (state, action) => {
            state.loading = action.payload;
        },
        setError: (state, action) => {
            state.error = action.payload;
        },
        setBillTableLastUpdate: (state, action) => {
            state.billTableLastUpdate = action.payload;
        },
    },
});

export const { setBills, setLoading, setError, setBillTableLastUpdate } =
    billsSlice.actions;
export default billsSlice.reducer;
