# Define the default make action
.PHONY: all
all: build-node build-nginx

# Define the action to build the Node Docker image
.PHONY: build-node
build-node:
	docker build -f Dockerfile.node -t your-username/nextjs-app:latest .

# Define the action to build the Nginx Docker image
.PHONY: build-nginx
build-nginx:
	docker build -f Dockerfile.nginx -t your-username/nginx-proxy:latest .

# Define the action to run Docker Compose
.PHONY: up
up:
	docker-compose up --build

# Define the action to stop Docker Compose
.PHONY: down
down:
	docker-compose down

# Optional: Add a clean action to remove images
.PHONY: clean
clean:
	docker rmi your-username/nextjs-app:latest your-username/nginx-proxy:latest